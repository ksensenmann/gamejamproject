// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class BloodyStrike : ModuleRules
{
	public BloodyStrike(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
