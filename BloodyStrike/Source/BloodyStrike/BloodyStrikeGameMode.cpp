// Copyright Epic Games, Inc. All Rights Reserved.

#include "BloodyStrikeGameMode.h"
#include "BloodyStrikePlayerController.h"
#include "BloodyStrikeCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABloodyStrikeGameMode::ABloodyStrikeGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ABloodyStrikePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}