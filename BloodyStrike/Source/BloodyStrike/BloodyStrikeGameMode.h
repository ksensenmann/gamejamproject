// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BloodyStrikeGameMode.generated.h"

UCLASS(minimalapi)
class ABloodyStrikeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABloodyStrikeGameMode();
};



