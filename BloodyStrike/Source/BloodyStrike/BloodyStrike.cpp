// Copyright Epic Games, Inc. All Rights Reserved.

#include "BloodyStrike.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BloodyStrike, "BloodyStrike" );

DEFINE_LOG_CATEGORY(LogBloodyStrike)
 